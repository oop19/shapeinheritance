/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author a
 */
public class Square extends Rectangle {

    public Square(double w, double l) {
        super(w, l);
    } 
    
    @Override
    public void print(){
        System.out.println("Square Side: "+ this.w+" Side: "+ this.l +" Area = "+calArea());
    }
}
