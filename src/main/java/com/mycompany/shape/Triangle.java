/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author a
 */
public class Triangle extends Shape{
   private double h,bl;
   public double f = 1.0/2;
   public Triangle(double h, double bl){
       this.h = h;
       this.bl = bl;
   }
   
   @Override
   public double calArea(){
       return f * h * bl;
   }
   
   @Override
   public void print(){
        System.out.println("Triangle Hight: "+ this.h+" Base: "+ this.bl +" Area = "+calArea());
    }
}
