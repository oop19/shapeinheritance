/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape;

/**
 *
 * @author a
 */
public class Rectangle extends Shape{
    double w;
    double l;
    public Rectangle(double w,double l){
        this.w = w;
        this.l = l;
    }
    
    @Override
    public double calArea(){
        return w * l;
    }
    
    @Override
    public void print(){
        System.out.println("Rectangle Width: "+ this.w+" Length: "+ this.l +" Area = "+calArea());
    }
}
